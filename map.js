
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBU_WnsMybxDuE7g90DUBLWfCd9VzqNpcA&libraries=places&callback=initMap">
///////

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        mapTypeControl: false,
        center: { lat: 32.08527, lng: 34.78262 },
        zoom: 13
    });

    var directionsDisplay ;
    //var geocoder = new google.maps.Geocoder();
    new AutocompleteDirectionsHandler(map);
     geocoder = new google.maps.Geocoder();
    document.getElementById('poligon').addEventListener('click', function() {
        SetBordersOrgCityByName(geocoder, map);
      });

} 
var geocoder;
var isSamecity;
var isfirstTime=true;
var placeIdCity;
var checkpLaceIdCity='1';
var isReurnTrue;
var isSamePlace=0;
var checkIfAllRouteInSquare;
var isOnesquare;
var latDest;
var lngDest;
/**
 * @constructor
*/

function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'WALKING';
    var originInput = document.getElementById('origin-input');
    var destinationInput = document.getElementById('destination-input');
    var modeSelector = document.getElementById('mode-selector');
    this.directionsService = new google.maps.DirectionsService;
   directionsDisplay = new google.maps.DirectionsRenderer({
    polylineOptions: {
      strokeColor: "blue"
    }});
   directionsDisplay.setMap(map);
      var originAutocomplete = new google.maps.places.Autocomplete(
            originInput,{ placeIdOnly: false });
    var destinationAutocomplete = new google.maps.places.Autocomplete(
        destinationInput, { placeIdOnly: false });//was true
    this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
    this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(destinationInput);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(modeSelector);
}

var colorStroke;
var contentReason;
AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function (autocomplete, mode) {
    var me = this;
    autocomplete.bindTo('bounds', this.map);
    autocomplete.addListener('place_changed', function () {
       var place = autocomplete.getPlace();
        if(!place)
         alert('No place return'+ place);
       console.log("place");
       console.log(place);
        if (!place.place_id) {
            window.alert(" נא בחר אופציה מהרשימה");//Please select an option from the dropdown list.
            return;
        }
        checkIfAllRouteInSquare=0; 
        isOnesquare=0;
        contentReason='';
        isSamePlace=0;
        var  longNameOrgCity="";
            //Get Org Name
            for (var i = 0; i < place.address_components.length; i++) {
                 if (place.address_components[i].types[0] == "locality") {
                    longNameOrgCity = place.address_components[i].long_name;
                     console.log(longNameOrgCity);
                     break;
                 }
             }
        if (mode === 'ORIG') {
            me.originPlaceId = place.place_id;
            OrgMinLngPoint='';
            OrgMaxLngPoint='';
            OrgMinLatPoint='';
            OrgMaxLatPoint='';
            SetBordersOrgCityByName(longNameOrgCity,mode);
        } 
        else
        { 
            me.destinationPlaceId = place.place_id;
            var latDest=place.geometry.location.lat();
            var lngDest= place.geometry.location.lng();
            checkIfAllRouteInSquare= checkIfPointInSquare(latDest,lngDest);
           if( me.originPlaceId==me.destinationPlaceId){
               isSamePlace=1;
           }
           if(checkIfAllRouteInSquare==1||isSamePlace==1)
           {
              colorStroke='green'; 
              contentReason='יעד ומוצא בתוך ריבוע העיר';
              alert('סיבה: ' +contentReason);
            }
            else{
               DestMinLngPoint='';
               DestMaxLngPoint='';
               DestMinLatPoint='';
               DestMaxLatPoint='';
               FirstSetBordersDestCityByName(longNameOrgCity,mode);
            }
        }
        me.route();
    });

};
latlngRouteArray=new Array();
var flag;
var meet2square;
var meet2squareInLoop;
var continueFrom;
var NameCity;
 AutocompleteDirectionsHandler.prototype.route = function () {
    if (!this.originPlaceId || !this.destinationPlaceId) {
        return;
    }
    var me = this;
    var isGood=false;
    this.directionsService.route({
        origin: { 'placeId': this.originPlaceId },
        destination: { 'placeId': this.destinationPlaceId },
        travelMode: this.travelMode,
        provideRouteAlternatives: true
    },async function (response, status) {
        if (status === 'OK') {
            console.log("response");
            console.log(response);
            contentReason='';
             meet2square=checkIf2SquareMeet('DEST');//if org and dest are meeting
             continueFrom=0;//keep j for next while
             meet2squareInLoop=1;//check if org city and other city meet in way
            if (meet2square==1){
               colorStroke='green';
               contentReason='ריבוע עיר יעד וריבוע עיר מוצא מתחברים לריבוע עיר אחת';
               alert('סיבה: ' +contentReason);
            }
            var k=0 ;
           // for (k = 0, len = response.routes.length; k < len; k++) {
                var stepsResponse=response.routes[k].legs[0].steps;//the steps points route to check
                console.log("stepsResponse");
                console.log(stepsResponse);
                var stepsResponse2=response.routes[0].legs[0].steps;
                    //var f=0;
                    if(k==0)
                    {
                      var infowindow = new google.maps.InfoWindow();
                      var marker, d;
                      for(var d=0;d<stepsResponse2.length;d++)
                      {
                        var latlng1={lat:response.routes[k].legs[0].steps[d].lat_lngs[0].lat(),lng:response.routes[k].legs[0].steps[d].lat_lngs[0].lng()};
                        marker = new google.maps.Marker({
                           position: latlng1,
                            map: me.map
                         });
                         google.maps.event.addListener(marker, 'click', (function(marker, d) {
                           return function() {
                             infowindow.setContent('<div><strong>'+stepsResponse2[d].instructions +'</strong><br><span>'+ stepsResponse2[d].distance.value+'מטר' +stepsResponse2[d].duration.text+'</span></div>');
                             infowindow.open(me.map, marker);
                           }
                         })(marker, d));
                      }
                    }
                    // setInstructions(stepsResponse);
                    //if the dest is not in org square and the wo square is not meeting so check the steps 
                    //from out of square
                    if(checkIfAllRouteInSquare==0&&isSamePlace==0&&meet2square==0)
                    {
                        var latlng;
                        var stepLatPoint;
                        var stepLngPoint;
                        var checkIStepInSquare=1;
                        var checkReversPointInsquare;
                        var i;
                        var lenghArr;
                        //var arr=new Array();
                        //all points in the way
                      /*  for(var g=0;g<response.routes[k].overview_path.length;g++){
                            var latlng={lat:response.routes[k].overview_path[g].lat(),lng:response.routes[k].overview_path[g].lng()};
                             arr.push(latlng);
                        }
                        console.log(".overview_path");
                        console.log(arr);*/
                        NameCity='';
                        lenghArr=0;
                        while(meet2squareInLoop==1&&meet2square==0&&NameCity!='none')
                        {
                            checkIStepInSquare=1;
                            //for(i=1;i<stepsResponse.length&&checkIStepInSquare==1;i+=5)
                            for(i=continueFrom+1;i<stepsResponse.length&&checkIStepInSquare==1;i+=10)
                            {
                              stepLatPoint=stepsResponse[i].start_point.lat();
                              stepLngPoint=stepsResponse[i].start_point.lng();
                              checkIStepInSquare= checkIfPointInSquare(stepLatPoint,stepLngPoint);
                              console.log("checkIStepInSquare:");
                              console.log(checkIStepInSquare);
                           }
                               lenghArr=i-9;
                           checkReversPointInsquare=0;
                        if(lenghArr>0)
                        {
                          for(j=i-5;j>=lenghArr&&checkReversPointInsquare==0;j--)
                           {
                              stepLatPoint=stepsResponse[j].start_point.lat();
                              stepLngPoint=stepsResponse[j].start_point.lng();
                              checkReversPointInsquare= checkIfPointInSquare(stepLatPoint,stepLngPoint);
                              console.log("checkReversPointInsquare after revers: ");
                              console.log(checkReversPointInsquare);
                           } 
                         }
                          // console.log('stepsResponse[j] the first point out of square');
                           console.log(stepsResponse[j+2].start_point.lat());
                           console.log(stepsResponse[j+2].start_point.lng());
                           var latPoint={lat:stepsResponse[j+2].start_point.lat(),lng:stepsResponse[j+2].start_point.lng()}
                            NameCity = await geocodeLatLng(latPoint);
                            console.log("NameCity");
                            console.log(NameCity);
                            if(NameCity!='none')//&&undefined//NameCity!='none'=out of square not in city
                            {
                             CityMinLngPoint='';
                             CityMaxLngPoint='';
                             CityMinLatPoint='';
                             CityMaxLatPoint='';
                             var isSet=await SetBordersDestCityByName(NameCity,'NEXTCITY');
                             console.log(isSet);
                              //che1ck if city's way point square is in org square
                              meet2squareInLoop=checkIf2SquareMeet('INWAY');
                              if(meet2squareInLoop==1){
                              setOrgBorders('','INCREASESQUARE');
                              //Check if dest square is in Increase org new square 
                              meet2square=checkIf2SquareMeet('DEST');
                              }
                            }
                            else if(NameCity=='none')
                               meet2squareInLoop=0;
                               continueFrom=j+2;//stepsResponse[j];
                        }
                        
                        if(meet2squareInLoop==1)
                        {
                            alert('why out? ');
                            colorStroke='green';
                        }
                        if(meet2square==1){
                            contentReason='ריבוע עיר יעד וריבוע עיר מוצא מתחברים לריבוע עיר אחת';
                            colorStroke='green';
                            alert('סיבה: ' +contentReason);
                        }
                        else if(meet2squareInLoop==0){
                        var km= measureRouteFromOutSquare(stepsResponse,continueFrom);
                        if(km>1200){
                            colorStroke='red';
                            contentReason='מעבר ל200 אמה מחוץ לעיר מוצא';
                            alert('סיבה: ' +contentReason);
                         }
                        else{
                            colorStroke='green';
                            contentReason='פחות מ 2000 אמה מחוץ לעיר מוצא';
                            alert('סיבה: ' +contentReason);
                         } 
                        } 
                        directionsDisplay.setMap(null);
                        directionsDisplay.setOptions({
                        polylineOptions: {
                            strokeColor: colorStroke
                        }
                        });
                        directionsDisplay.setMap(me.map);
                    
                    } 
                    directionsDisplay.setMap(null);
                      directionsDisplay.setOptions({
                      polylineOptions: {
                        strokeColor: colorStroke
                      }
                     });
                    directionsDisplay.setMap(me.map);
                   
               // }//for route
                
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
   /* var checksamecityreurn=  checkSameCity(me.originPlaceId,me.destinationPlaceId);
    window.alert("checksamecityreurn: "+checksamecityreurn);*/
};
function geocodeLatLng(latlngstep) {
    return new Promise((resolve, reject) => {
      geocoder.geocode({'location': latlngstep}, function(results, status) {
        if (status === 'OK') {
          if (results[0]) {
            NameCity='none';
              for (var i = 0; i < results[0].address_components.length; i++) {
                  if (results[0].address_components[i].types[0] == "locality") {
                      NameCity = results[0].address_components[i].long_name;
                      break;
                  }
              }
              flag=1;
              resolve(NameCity);
          } else {
            window.alert('No results found');
            flag=2;
            reject(flag);
          }
         // resolve(flag);
       } else {
          window.alert('Geocoder failed due to: ' + status);
          flag=2;
          reject(flag);
        }
      });
    });
  }
 
function doGeo(results){
    if (results[0]) {
        for(var j=0;j<results.length;j++){
          /*  if(isSamecity==0){
                window.alert("break for !!!!!!!!!!!!");
                break;
            }*/
            if( results[j].types[0]=='locality'){
                placeIdCity=results[j].place_id;
            console.log("address: "+ results[j]);
                if(isfirstTime==true)
                {
                  checkpLaceIdCity=placeIdCity;
                  isfirstTime=false;
                }
                if(checkpLaceIdCity==placeIdCity)
                  isSamecity=1;//true
                else
                  {
                      isSamecity=0;//false
                      window.alert("return isSamecity "+isSamecity);
                      console.log(placeIdCity);
                      console.log(isSamecity);
                      return isSamecity;
                      
                    }
                  
                console.log("address2: "+  results[j]);
                console.log(placeIdCity);
                console.log(isSamecity);
        }
    }
       
            return isSamecity;
        } else {
        window.alert('No results found');
        }
        return 2;//no results
}

function GetOrgCity(placIdOrg){
    longNameOrgCity="";

}

function calculateDistanse(distanse) {

}

function getThePointoutOfTown(route) {
}
//32.085997,34.832079  ר עקיבא
//32.066081,34.834558  לחי רמת גן ישראל
//32.07922,34.881898 מרכז רפואי רבין השרון
var stepLatPoint=32.085997;
var stepLngPoint=34.832079;
var OrgMinLngPoint;
var OrgMaxLngPoint;
var OrgMinLatPoint;
var OrgMaxLatPoint;
var DestMinLngPoint;
var DestMaxLngPoint;
var DestMinLatPoint;
var DestMaxLatPoint;
var CityMinLngPoint;
var CityMaxLngPoint;
var CityMinLatPoint;
var CityMaxLatPoint;

//get or const longNameOrgCity notice not be garbage.set as "" if need.
// resultscity=new Array();


function ChangeColorRoute(map,color){
    directionsDisplay.setMap(null);
    directionsDisplay.setOptions({
      polylineOptions: {
        strokeColor: color
      }
    });

    directionsDisplay.setMap(map);
}
//set the city's borders for org city using geocode
function SetBordersOrgCityByName(cityName,mode) {//, resultsMap
    var address=cityName;
   if(address&&address!="")
     {
      //  var geocoder = new google.maps.Geocoder();
         geocoder.geocode({'address': address}, function(results, status) {
           if (status === 'OK') {
               console.log("results[0]");
               //to earase
               console.log(results[0]);
               var tryBounds;
               if(results[0].geometry.bounds)
                tryBounds=results[0].geometry.bounds;
               else if(results[0].geometry.viewport)
                 tryBounds=results[0].geometry.viewport;
               if(tryBounds)
                 setOrgBorders(tryBounds,mode);
               else
                 alert('tryBounds is ampty '+tryBounds + ' '+'mode'+mode);
           } else {
             alert('Geocode was not successful for the following reason: ' + status);
           }
         });
    }
    else
     alert("No adress");
  }
  function FirstSetBordersDestCityByName(cityName,mode) {//, resultsMap
    var address=cityName;
   if(address!="")
     {
       //  return new Promise((resolve,reject)=>{
          geocoder.geocode({'address': address}, function(results, status) {
           if (status === 'OK') {
              /* console.log("dest before set borders");
               console.log(results[0].geometry.bounds.b.b,results[0].geometry.bounds.b.f,results[0].geometry.bounds.f.b,results[0].geometry.bounds.f.f);
               */
              if(results[0].geometry.bounds)
               var trySetBorders=results[0].geometry.bounds;
               else if(results[0].geometry.viewport)
                 var trySetBorders=results[0].geometry.viewport;
                if(trySetBorders){
                 setOrgBorders(trySetBorders,mode);
                 var SetBorder=1;
                }
                else
                  alert('no bounds: '+trySetBorders+' mode '+mode);
           // resolve(SetBorder);
           } else {
             alert('Geocode was not successful for the following reason: ' + status);
             var SetBorder=2;
             //reject(SetBorder);
           }
         });
        // }
        //)
    
    }
  }

function SetBordersDestCityByName(cityName,mode) {//, resultsMap
    var address=cityName;
   if(address!="")
     {
         return new Promise((resolve,reject)=>{
          geocoder.geocode({'address': address}, function(results, status) {
           if (status === 'OK') {
              /* console.log("dest before set borders");
               console.log(results[0].geometry.bounds.b.b,results[0].geometry.bounds.b.f,results[0].geometry.bounds.f.b,results[0].geometry.bounds.f.f);
               */
              if(results[0].geometry.bounds)
               var setborderDest=results[0].geometry.bounds;
              else if(results[0].geometry.viewport)
                var setborderDest=results[0].geometry.viewport;
             if(setborderDest){
            setOrgBorders(setborderDest,mode);
            var SetBorder=1;
             }
             else
              alert('bounds is empty. setborderDest= '+setborderDest,'mode='+mode);
            resolve(SetBorder);
           } else {
             alert('Geocode was not successful for the following reason: ' + status);
             var SetBorder=2;
             reject(SetBorder);
           }
         });
         }
        )
    
    }
  }
  var try1;
  //Get point lng-lat and check if it's in the city's Square
function setOrgBorders(bounds,mode){
    if (mode === 'ORIG'){
    OrgMinLngPoint=bounds.b.b;
    OrgMaxLngPoint=bounds.b.f;
    OrgMinLatPoint=bounds.f.b;
    OrgMaxLatPoint=bounds.f.f;
    console.log(OrgMinLngPoint,OrgMaxLngPoint,OrgMinLatPoint,OrgMaxLatPoint);
    //setOrgNewBorders(mode);
}
    else if (mode === 'DEST'){
    DestMinLngPoint=bounds.b.b;
    DestMaxLngPoint=bounds.b.f;
    DestMinLatPoint=bounds.f.b;
    DestMaxLatPoint=bounds.f.f;
    console.log(DestMinLngPoint,DestMaxLngPoint,DestMinLatPoint,DestMaxLatPoint);
    }
//if its acity in the way
else if(mode=='NEXTCITY'){
   
   CityMinLngPoint=bounds.b.b;
   CityMaxLngPoint=bounds.b.f;
   CityMinLatPoint=bounds.f.b;
   CityMaxLatPoint=bounds.f.f;
   console.log(CityMinLngPoint,CityMaxLngPoint,CityMinLatPoint,CityMaxLatPoint);
}
else if(mode === 'INCREASESQUARE'){
  if(OrgMinLngPoint&&OrgMaxLngPoint&&OrgMinLatPoint&&OrgMaxLatPoint&&CityMinLngPoint&&CityMaxLngPoint&&CityMinLatPoint&&CityMaxLatPoint){
    OrgMinLngPoint=OrgMinLngPoint<CityMinLngPoint?OrgMinLngPoint:CityMinLngPoint;
    OrgMaxLngPoint=OrgMaxLngPoint>CityMaxLngPoint?OrgMaxLngPoint:CityMaxLngPoint;
    OrgMinLatPoint=OrgMinLatPoint<CityMinLatPoint?OrgMinLatPoint:CityMinLatPoint;
    OrgMaxLatPoint=OrgMaxLatPoint>CityMaxLatPoint?OrgMaxLatPoint:CityMaxLatPoint;
    console.log('next city borders');
    console.log(OrgMinLngPoint,OrgMaxLngPoint,OrgMinLatPoint,OrgMaxLatPoint);
 }
 else
   alert('not set parameters, mode: '+mode);
}
}
function setOrgNewBorders(mode){
      //calc the city's borders +42.40 meters
        /*
            new_latitude  = latitude  + (dy / r_earth) * (180 / pi);
            new_longitude = longitude + (dx / r_earth) * (180 / pi) / cos(latitude * pi/180);
        */
      var meters = 42.40;//קרפף העיר
      var coef = meters * 0.0000089;
      if (mode === 'ORIG'){
        OrgMinLngPoint=OrgMinLngPoint-coef/ Math.cos(OrgMinLatPoint * 0.018);
        OrgMaxLngPoint=OrgMaxLngPoint+coef/ Math.cos(OrgMaxLatPoint * 0.018);
        OrgMinLatPoint=OrgMinLatPoint-coef;
        OrgMaxLatPoint=OrgMaxLatPoint+coef;
        console.log("New Org lat lng");
        console.log(OrgMinLngPoint,OrgMaxLngPoint,OrgMinLatPoint,OrgMaxLatPoint);
      }
      else if (mode === 'DEST'){
        DestMinLngPoint=DestMinLngPoint-coef/ Math.cos(OrgMinLatPoint * 0.018);
        DestMaxLngPoint=DestMaxLngPoint+coef/ Math.cos(OrgMaxLatPoint * 0.018);
        DestMinLatPoint=DestMinLatPoint-coef;
        DestMaxLatPoint=DestMaxLatPoint+coef;
        console.log("New Dest lat lng");
        console.log(DestMinLngPoint,DestMaxLngPoint,DestMinLatPoint,DestMaxLatPoint);
      }
}
function checkIf2SquareMeet(compareTo){
    if(OrgMaxLngPoint&&OrgMinLngPoint&&OrgMaxLatPoint&&OrgMinLatPoint){
    if(compareTo=='DEST')
    {
        if(DestMinLngPoint&&DestMaxLngPoint&&DestMinLatPoint&&DestMaxLatPoint){
     if(DestMinLngPoint>OrgMaxLngPoint||DestMaxLngPoint<OrgMinLngPoint||DestMinLatPoint>OrgMaxLatPoint||DestMaxLatPoint<OrgMinLatPoint)
      return 0;
     else
      return 1;
      }
    else
     alert('not set dest parameters');
    }
    else if(compareTo=='INWAY')
    {
        if(CityMinLngPoint&&CityMaxLngPoint&&CityMinLatPoint&&CityMaxLatPoint){
      if(CityMinLngPoint>OrgMaxLngPoint||CityMaxLngPoint<OrgMinLngPoint
        ||CityMinLatPoint>OrgMaxLatPoint||CityMaxLatPoint<OrgMinLatPoint)
       return 0;
      else
       return 1;
      }
      else
       alert('not set dest parameters');
    }
    }
    else
      alert('not set org parameters');
}
  function checkIfPointInSquare(locationLat,locatioLng){
if(OrgMinLatPoint&&OrgMaxLatPoint&&OrgMinLngPoint&&OrgMaxLngPoint&&locationLat&&locatioLng){   
    if(locationLat<OrgMinLatPoint||locationLat>OrgMaxLatPoint||locatioLng<OrgMinLngPoint||locatioLng>OrgMaxLngPoint)
    return 0; //out of Square
else
    return 1; //in Square
}
else 
  alert('Not set org parametrs or locationLat locatioLng for checkIfPointInSquare');
}

function drawPolygon() {

    // stockholm boundaries
    var boundaries = '17.73966,59.66395|17.75651,59.61827|17.81771,59.58645|17.84347,59.53069|17.78465,59.49340|17.78277,59.38916|17.83277,59.36500|17.93215,59.33652|18.00736,59.34496|18.09139,59.33444|18.05722,59.39139|18.12000,59.45388|18.19508,59.45012|18.16562,59.41083|18.32826,59.39795|18.28659,59.41228|18.25945,59.44638|18.31722,59.47361|18.37333,59.46694|18.66638,59.59138|18.69979,59.64409|18.74555,59.68916|18.84139,59.71249|18.98569,59.71666|19.03222,59.71999|19.07965,59.76743|18.93472,59.78361|18.86472,59.79804|18.96922,59.86561|19.06545,59.83250|19.07056,59.88972|19.00722,59.91277|18.92972,59.92472|18.89000,59.95805|18.81722,60.07548|18.77666,60.11084|18.71139,60.12806|18.63277,60.14500|18.50743,60.15265|18.44805,59.99249|18.37334,59.86500|18.29055,59.83527|18.15472,59.79556|18.08111,59.74014|17.96055,59.70472|17.89694,59.68916|17.73966,59.66395';

    var latLngArray = boundaries.split('|');
    var points = [];
    for (var i = 0; i < latLngArray.length; i++) {
        pos = latLngArray[i].split(',');
        points.push(new google.maps.LatLng(parseFloat(pos[1]), parseFloat(pos[0])));
    }

    var shape = new google.maps.Polygon({
        paths: points,
        strokeColor: '#ff0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#ff0000',
        fillOpacity: 0.35
    });

    shape.setMap(map);
}

/*
$(document).ready(function () {
    initialize();
});

$(document).on('click', '.show-address', function (e) {
    e.preventDefault();
    showAddress();
});

$(document).on('click', '.draw-address', function (e) {
    e.preventDefault();
    drawPolygon();
});*/

function getPlaceDetails(placeId) {
    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);

    service.getDetails({

    }, function (place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            var marker = new google.maps.Marker({
                map: map,
                position: place.geometry.location
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                    'Place ID: ' + place.place_id + '<br>' +
                    place.formatted_address + '</div>');
                infowindow.open(map, this);
            });
        }
    });
}

function showSteps(directionResult, markerArray, stepDisplay, map) {
    // For each step, place a marker, and add the text to the marker's infowindow.
    // Also attach the marker to an array so we can keep track of it and remove it
    // when calculating new routes.
    var myRoute = directionResult.routes[0].legs[0];
    for (var i = 0; i < myRoute.steps.length; i++) {
        var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
        marker.setMap(map);
        marker.setPosition(myRoute.steps[i].start_location);
        attachInstructionText(
            stepDisplay, marker, myRoute.steps[i].instructions, map);
    }
}
//measure route 
function measureRouteFromOutSquare(steps,start){
    var i=start;
    var total = 0;
    var routeOutSquare=steps;
    for(;i<routeOutSquare.length;i++)
    {
        total+=routeOutSquare[i].distance.value;
    }
    return total;
}

function setInstructions(steps){
var stepsArr=steps;
/*
var container = document.createElement("div");
  var text = "פנה ימינה";
  var blockDiv, textSpan;  // used in the for loop
  container.className = "instruction";
  document.getElementById('instructionssWrapper').appendChild(container);

  for(var i = 0; i < stepsArr.length; i++) {
    var text = stepsArr[i].instructions;
    console.log("text");
    console.log(text);
    blockDiv = document.createElement("div");
    blockDiv.className = "inInstruction";
    //textSpan = document.createElement("span");
   // textSpan.append(text);  // see note about browser compatibility
    blockDiv.append(stepsArr[i].instructions);
    container.append(blockDiv);
  }*/
/*var element = document.createElement("div");
element.appendChild(document.createTextNode('המשך ישר'));
document.getElementById('instructionssWrapper').appendChild(element);*/
}
function computeTotalDistance(result) {
    var total = 0;
    var myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
        total += myroute.legs[i].distance.value;
    }
    total = total / 1000;
    document.getElementById('total').innerHTML = total + ' km';
}

function geoLocation() {
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}


function checkSameCity(orgPlaceId, destPlaceId) {
    //
    longNameOrgCity="";
    var service = new google.maps.places.PlacesService(map);
    //var cityOrg = "";
    var cityDest = "";
    service.getDetails({
        placeId: orgPlaceId
    }, function (place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            alert(place.formatted_address);
            console.log("place: "+place);
            console.log(place.address_components);
            for (var i = 0; i < place.address_components.length; i++) {
               ///debugger;
                if (place.address_components[i].types[0] == "locality") {
                    cityOrg = place.address_components[i].long_name;
                    longNameOrgCity=cityOrg;
                    console.log(cityOrg);
                    break;
                }
            }
        }
    });
    service.getDetails({
        placeId: destPlaceId
    }, function (place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            alert(place.formatted_address);
            console.log(place.address_components);
            for (var i = 0; i < place.address_components.length; i++) {
                //debugger;
                if (place.address_components[i].types[0] == "locality") {
                    cityDest = place.address_components[i].long_name;
                    console.log(cityDest);
                    break;
                }
            }
        }
    });
    if (cityOrg == cityDest && cityOrg != "")
         window.alert("true");
    window.alert("false");
       // return true;
    //return false;
}


 